<?php
// HTTP
define('HTTP_SERVER', 'http://www.tiendacosademinas.com/');

// HTTPS
define('HTTPS_SERVER', 'http://www.tiendacosademinas.com/');

// DIR
define('DIR_APPLICATION', '/Applications/XAMPP/xamppfiles/htdocs/tiendacosademinas/catalog/');
define('DIR_SYSTEM', '/Applications/XAMPP/xamppfiles/htdocs/tiendacosademinas/system/');
define('DIR_IMAGE', '/Applications/XAMPP/xamppfiles/htdocs/tiendacosademinas/image/');
define('DIR_LANGUAGE', '/Applications/XAMPP/xamppfiles/htdocs/tiendacosademinas/catalog/language/');
define('DIR_TEMPLATE', '/Applications/XAMPP/xamppfiles/htdocs/tiendacosademinas/catalog/view/theme/');
define('DIR_CONFIG', '/Applications/XAMPP/xamppfiles/htdocs/tiendacosademinas/system/config/');
define('DIR_CACHE', '/Applications/XAMPP/xamppfiles/htdocs/tiendacosademinas/system/storage/cache/');
define('DIR_DOWNLOAD', '/Applications/XAMPP/xamppfiles/htdocs/tiendacosademinas/system/storage/download/');
define('DIR_LOGS', '/Applications/XAMPP/xamppfiles/htdocs/tiendacosademinas/system/storage/logs/');
define('DIR_MODIFICATION', '/Applications/XAMPP/xamppfiles/htdocs/tiendacosademinas/system/storage/modification/');
define('DIR_UPLOAD', '/Applications/XAMPP/xamppfiles/htdocs/tiendacosademinas/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'tiendacosademinas');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

<div class="category-module">
	<div class="module-title">
		<h2><?php echo $heading_title; ?></h2>
	</div>
<ul class="list-unstyled category-list">
  <?php foreach ($categories as $category) { ?>
	<li>
	  <a href="<?php echo $category['href']; ?>" class="<?php echo ($category['category_id'] == $category_id) ? ' active' : '' ?>"><?php echo $category['name']; ?></a>
		  <?php if ($category['children']) { ?>
				<i class="<?php echo ($category['category_id'] == $category_id) ? 'pe-7s-angle-down ttopen' : 'pe-7s-angle-right ttclose' ?>"></i>
				<ul class="list-unstyled category-sub-list" <?php echo ($category['category_id'] == $category_id) ? 'style="display: block"' : 'style="display: none"' ?>>
				  <?php foreach ($category['children'] as $child) { ?>
					<li>
					  <?php if ($child['category_id'] == $child_id) { ?>
							<a href="<?php echo $child['href']; ?>" class="active"><?php echo $child['name']; ?></a>
					  <?php } else { ?>
							<a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
					  <?php } ?>
					  </li>
				  <?php } ?>
				</ul>
		  <?php } ?>
	 </li>
  <?php } ?>
</ul>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.category-module li > i').click(function() {
		if ($(this).hasClass('ttopen')) {varche = true} else {varche = false};
		if(varche == false){
			$(this).toggleClass("pe-7s-angle-right pe-7s-angle-down");
			$(this).addClass("ttopen");
			$(this).removeClass("ttclose");
			$(this).parent().find('ul').slideDown();
			varche = true;
		} else 
		{	
			$(this).toggleClass("pe-7s-angle-down pe-7s-angle-right");
			$(this).removeClass("ttopen");
			$(this).addClass("ttclose");
			$(this).parent().find('ul').slideUp();
			varche = false;
		}
		});
	});
</script>

<div class="search-container">
	<i class="pe-7s-search"></i>
	<div id="search" class="input-group">			
		<input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-lg" />
		<!-- <i class="pe-7s-search"></i> -->
		<button type="button" class="btn btn-default btn-lg"><i class="pe-7s-search"></i></button>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.search-container > i').click(function(){
			$("#search").slideToggle();
			$(this).toggleClass("pe-7s-search pe-7s-close")
		});
		$("#search > input").on("change paste keyup", function() {
		   $('#search > button').toggleClass('expand-search2');
		});
	});
</script>
<?php if(isset($block2)){ echo $block2; } ?>
<footer> 
	<div class="container">
	<?php if(isset($block3)){ echo $block3; } ?>
	<div class="middle-footer row">
	<?php if(isset($block4)){ ?>
	<div class="col-sm-4">
		<?php echo $block4; ?>
	</div>
	<?php } ?>
	<div class="col-sm-2">
        <h3 class="footer-title"><?php echo $text_extra; ?></h3>
        <ul class="list-unstyled">
          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
          <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
		  <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
        </ul>
      </div>
	<?php if(isset($block5)){ ?>
		<div class="col-sm-2 position-4-socialfooter">
			<?php echo $block5; ?>
		</div>
	<?php } ?>
	<?php if(isset($block6)){ ?>
		<div class="col-sm-4 position-4-statictags">
			<?php echo $block6; ?>
		</div>
	<?php } ?>
	</div>
	</div>
	<div class="bottom-footer">
	<div class="container">
		<p class="text_powered"><?php echo $powered; ?></p>	
		<div class="payment-method"><a href="#"><img src="image/payment/payment.png" alt="image payment"></a></div>
		<div id="back-top"><i class="fa fa-angle-double-up"></i></div>
	</div>
	</div>
</footer>
<script type="text/javascript">
$(document).ready(function(){
	// hide #back-top first
	$("#back-top").hide();
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 300) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});
		// scroll body to 0px on click
		$('#back-top').click(function () {
			$('body,html').animate({scrollTop: 0}, 800);
			return false;
		});
	});
});
</script>
</div>
<!-- wrapper -->
<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>
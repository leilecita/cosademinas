<?php
// Heading
$_['heading_title'] = 'Most Viewed Products';
$_['sub_title'] = 'Browse the collection of our best selling and top interesting products.<br/>You’ll definitely find what you are looking for.';

// Text
$_['text_tax']      = 'Ex Tax:';
$_['text_sale']      = 'Sale';
$_['text_new']      = 'New';
$_['text_empty']    = 'There is no Most Viewed Products!';
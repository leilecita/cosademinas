<?php
// Heading
$_['heading_title'] = 'Featured Categories';
$_['sub_title'] = 'Variety of product categories, tens of products, only five-stars reviews.<br/>Browse the collections right now.';
$_['count_product'] = '%s product(s)';
$_['text_viewall'] = 'View All';
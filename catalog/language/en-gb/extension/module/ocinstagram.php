<?php
// Heading
$_['text_subtitle']    = 'Follow us on instagram';
$_['heading_title']    = 'Instagram';

$_['text_follow']        = 'Follow us on Instagram';
$_['text_des']        = 'View Our 2016 Lookbook Feed';
$_['text_copyright']        = 'Instagram -- &copy; %s';